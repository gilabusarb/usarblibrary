Best exotic brand: Lamborghini 
2019 Lamborghini Aventador SVJ
Lamborghini
Why should you buy this: To create a scene everywhere you drive.

Who’s it for: People who want to stand out and draw attention.

Why we picked Lamborghini:

Exotic cars demand head-turning attention and make their presence known to everyone in the area without feeling shame about it. No automaker does that better than the raging bull from Italy, Lamborghini. Ferraris, McLarens, and others also do this well, but not like a Lamborghini. The brand is known for screaming V10 and V12 engines mounted right behind the driver, and their menacing design language is polarizing, making nobody question it’s a Lambo. The flashiness continues inside, and models can be painted in striking colors with names like Verde Scandal, Nero Nemesis, Oro Elios, Arancio Borealis, and Balloon White. Lamborghini is also famed for more extreme, limited production models like the Sián and Centenario. 

Lamborghini currently produces the Huracán, which is powered by a 5.2-liter V10 that produces 602 horsepower and 413 pound-feet of torque. The upgraded Huracán Performante nets 29 more horsepower and 30 torque (631hp and 443 lb.-ft.) with the same V10 that revs to a sky-high 8,500 rpm. The Huracán Evo utilizes the same engine but is beefed up with many track-focused upgrades. The brand’s Aventador S model packs a 730hp, 507 lb.-ft., 6.5-liter V12, but the faster SVJ model takes power up a notch with 760hp and 531 lb.-ft. 

Lamborghini’s latest model is not a coupe or convertible — it’s an SUV. The Urus is the brand’s first high-riding vehicle, and it doesn’t use a 10- or 12-cylinder engine. Instead, a twin-turbo 4.0-liter V-8 does the job and makes 641hp and 627 lb.-ft. Like all Lambos, the Urus does not look conventional, and the fastback SUV easily stands out from all the other family haulers.